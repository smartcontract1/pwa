import {Component, OnInit} from '@angular/core';
import {AppletService} from '../_services/applet.service';
import {Router, ActivatedRoute,NavigationEnd} from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit{
	list = [];
	tabs = 1;
	statuses = [
		{state: 'ALL', title : 'Все'},
		{state: 'DRAFT', title : 'Черновик'},
		{state: 'IN_PROGRESS', title : 'На подписании'},
		{state: 'CANCELED', title : 'Отклонен'},
		{state: 'SUCCESS', title : 'Подписан'},
	]

	types = [
		{state: 'SUCCESS,IN_PROGRESS', title : 'Все'},
		{state: 'IN_PROGRESS', title : 'На подписании'},
		{state: 'FAILED', title : 'Отклонен'},
		{state: 'SUCCESS', title : 'Подписан'},
	]

	filter = {
		state: {
			status : 'ALL',
			type : 'SUCCESS,IN_PROGRESS'
		},
	}

  filterShow = false;
  currentLanguage;
  loading = true;
  
	constructor(
    public app: AppletService,
    public router: Router,
    public translate: TranslateService,
    private activatedRoute: ActivatedRoute, 
  ){
    this.app.getCookie('user_data');
    this.currentLanguage = this.translate.currentLang || this.translate.defaultLang;
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
    });
    	for(let item of this.statuses){
    		this.translate.get(item.state).subscribe(
		    	translation => {
		        item.title = translation;
		    });
    	}
    	for(let item of this.types){
    		this.translate.get(item.state).subscribe(
		    	translation => {
		        item.title = translation;
		    });
    	}

    	router.events.subscribe((val) => {
	        if(val instanceof NavigationEnd){
	        	this.list = [];
	        	this.tabs = this.activatedRoute.snapshot.params['tab'];
				if(this.tabs){
					this.tabs = this.tabs * 1;
				}
				if(this.tabs && this.tabs === 2){
					this.getContractList();
				}else{
					this.tabs = 1;
					this.getContractList();
				}
	        }
	    });
	}

	ngOnInit(){
		
	}

	getContractList(){
		let link = this.tabs === 1 ? 'initiator' : 'party';
		let filter;
		if(this.tabs === 1){
			filter = this.filter.state.status;
		}else{
			filter = this.filter.state.type;
		}
		let settings = '?start=0&desc=true&limit=50&state=' + filter;
		this.app.get(`/contracts/read/${link}/` + (this.app.user && this.app.user.iin) + settings)
	    .subscribe(res => {
	    	this.list = res;
			this.filterShow = false;
			this.loading = false;
	    },error => {
	    	this.loading = false;
	    });
	}

	showFilter(){
		this.filterShow = true;
	}

	closeFilter(){
		this.getContractList();
	}

	resetFilter(){
		this.filter.state.status = 'ALL';
		this.filter.state.type = 'SUCCESS,IN_PROGRESS';
	}

	swipeLeft(){
		this.router.navigate(['/main/2']);
	}
	swipeRight(){
		this.router.navigate(['/main/1']);
	}
}