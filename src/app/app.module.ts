import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule , HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { AppletService,  } from './_services/applet.service';

import { MainComponent } from './main/main.component';
import { FaqComponent } from './faq/faq.component';
import { MainListComponent } from './mainlist/mainlist.component';
import { ContractComponent } from './contract/contract.component';
import { TemplateComponent} from './template/template.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import {ShowComponent} from './show/show.component';
import {InvitationShowComponent} from './invitationShow/show.component';
import {PartiesComponent} from './parties/parties.component';

import { registerLocaleData} from '@angular/common/';
import ru from '@angular/common/locales/ru';

import { OnlyNumber } from './_share/onlynumbers.directive';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { ModalComponent } from './agreement/modal/modal.component';

import { SafeUrlPipe } from './_pipes/safe-url.pipe';

import {AgreementListComponent} from './agreement/list/agreement-list.component';

import { APatternRestrict } from '../assets/js/a-pattern-restrict';

declare var Hammer: any;

import { AuthInterceptor } from './_services/auth.interceptor';
import {NgxMaskModule} from 'ngx-mask';
import {NgbDateParserFormatter, NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {DateInputComponent} from './components/date-input/date-input.component';
import {MomentDateFormatter} from "./MomentDateFormatter";

export class HammerConfig extends HammerGestureConfig {

  buildHammer(element: HTMLElement) {
    let options = {};

    if (element.attributes['data-mc-options']) {
      try {
        const parseOptions = JSON.parse(element.attributes['data-mc-options'].nodeValue);
        options = parseOptions;
      } catch (err) {
        console.error('An error occurred when attempting to parse Hammer.js options: ', err);
      }
    }

    const mc = new Hammer(element, options);

    // keep default angular config
    mc.get('pinch').set({enable: true});
    mc.get('rotate').set({enable: true});

    // retain support for angular overrides object
    for (const eventName in this.overrides) {
      mc.get(eventName).set(this.overrides[eventName]);
    }

    return mc;
  }
}
registerLocaleData(ru);

const appRoutes: Routes = [
  {path: '', component: MainListComponent},
  {path: 'mainlist', component: MainListComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'main/:tab', component: MainComponent},
  {path: 'contract', component: ContractComponent},
  {path: 'edit/:templateId/:id', component: TemplateComponent},
  {path: 'template/:templateId', component: TemplateComponent},
  {path: 'show/:id', component: ShowComponent},
  {path: 'parties/:id', component: PartiesComponent},
  {path: 'invshow/:id', component: InvitationShowComponent},
  {path: 'modal/:id', component: ModalComponent},
  {path: 'agreement/:tab', component: AgreementListComponent},
];

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    FaqComponent,
    AppComponent,
    MainListComponent,
    MainComponent,
    ContractComponent,
    TemplateComponent,
    ShowComponent,
    PartiesComponent,
    InvitationShowComponent,
    OnlyNumber,
    ModalComponent,
    SafeUrlPipe,
    AgreementListComponent,
    APatternRestrict,
    DateInputComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbDatepickerModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgxMaskModule.forRoot(),
  ],
  providers: [
    AppletService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    // TODO: locale for date picker: kz, kk, kk-KZ, kk-KK, kz-KZ not works
    {provide: LOCALE_ID, useValue: 'ru' },
    {provide: HAMMER_GESTURE_CONFIG, useClass: HammerConfig },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: NgbDateParserFormatter,
      useClass: MomentDateFormatter
    },
  ],
  bootstrap: [AppComponent],
  schemas: [
      CUSTOM_ELEMENTS_SCHEMA,
  ],
  entryComponents: [
  ],
})
export class AppModule { }
