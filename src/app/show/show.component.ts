import {Component, OnInit, NgZone} from '@angular/core';
import {ActivatedRoute, Router, ParamMap} from '@angular/router';
import {AppletService} from '../_services/applet.service';
import { DomSanitizer} from '@angular/platform-browser';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import  '../../assets/js/custom.js';

declare function showReg01NextSteps(iin,queryNum, carCost, autoRegNum, autoRegId,contractId): any;
declare function showStatusPage(requestNumber, contractId, seller): any;
declare var exposedFunction;

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css'],
})

export class ShowComponent {
	id;
	contract;
	document;
	documentFileds = [];
	html;
	loading = false;
	deleteModal = false;
	showBtn = false;
	showStatusBtn = false;
	reqNumber;
	rkCode;
	showPayedText = false;
	showInfoText = false;
	currentLanguage;

	showPaymentText = false;
	inProgressText = false;
	signDocIng = false;
	modalError = false;
  errorText: string;
  errorDescription: string;

  constructor(
    private route: ActivatedRoute,
    public app: AppletService,
    public router: Router,
    private sanitizer: DomSanitizer,
    public translate: TranslateService
  ) {
    const lang = this.app.getCookie('lang');

    this.currentLanguage =  lang ? lang : 'ru';
    if (lang === 'en') {
      this.currentLanguage = 'ru';
    }
    this.app.getCookie('user_data');
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
      if (event.lang === 'en') {
        this.currentLanguage = 'ru';
      }
    });
		eval("window.webv=this;");
	}

	ngOnInit() {
		this.id = this.route.snapshot.params['id'];
		this.getContractById();
	}

	getContractById(){
		this.app.get('/contracts/' + this.id)
	    .subscribe(res => {
	    	this.contract = res;
			this.documentFileds = this.contract.inputForm.fields;
			this.getTemplateById(this.contract.documentId);
	    });
	}

	payC(){
		this.app.payWorkflowState(this.id).subscribe(res => {
			this.getContractByIdLoad();
		})
	}

	getContractByIdLoad(){
		this.loading = true;
		this.app.get('/contracts/' + this.id)
	    .subscribe(res => {
		    this.contract = res;
			// this.getWorkflowState();
			if(this.contract.currentState === 'VERIFICATION' || this.contract.currentState === 'PARTIES_SIGNATURE_VERIFICATION' || this.contract.currentState === 'WAITING_FOR_APPROVAL'){
				setTimeout(()=>{
					this.getContractByIdLoad();
			 	}, 3000);
			}else{
				this.loading = false;
				this.documentFileds = this.contract.inputForm.fields;
				this.getTemplateById(this.contract.documentId);
			}
	    });
	}


	getWorkflowState(){
		let showBtn = false;
		this.showStatusBtn = false;
		this.rkCode = null;
		this.showPayedText = false;
		let rk;
		this.inProgressText = false;
		this.app.getWorkflowState(this.id).subscribe(res => {
			if(res && res.data){
				for(let item of res.data.tasks){
					if(item.status === 'IN_PROGRESS' && item.taskType !== 'wait_for_initiator_sign' && item.taskType !== 'wait_for_acceptor_sign'){
						this.inProgressText = true;
					}
					if(item.taskType === 'wait_for_reg01_start'){
						if(item.status === 'COMPLETED'){
							this.reqNumber = item.outputData.request_number;
							this.showInfoText = true;
						}else if(item.status === 'IN_PROGRESS'){
							showBtn = true;
							this.showInfoText = true;
						}
					}

					if(item.taskType === 'wait_for_my_payment'){
						if(item.status === 'COMPLETED' || item.status === 'IN_PROGRESS'){
							this.showInfoText = false;
						}
					}

					if(item.taskType === 'wait_for_reg01_finish'){
						if(item.status === 'COMPLETED'){

						}else if(item.status === 'IN_PROGRESS'){
							this.showStatusBtn = true;
						}
					}

					if(item.taskType === 'get_rk_code'){
						if(item.status === 'COMPLETED'){
							rk = item.outputData.rk_code;
						}
					}


					if(item.taskType === 'wait_for_mu_payment'){
						if(item.status === 'IN_PROGRESS'){
							this.rkCode = rk;
						}else if(item.status === 'COMPLETED'){
							this.showPayedText = true;
						}
					}

					if(item.taskType ==='send_mu'){

					}
				}
				if(showBtn && !this.showStatusBtn){
					this.showBtn = true;
				}

				this.showPaymentText = true;
			}

		})
	}

	goToNextStep(){
		let newNumber = this.contract.inputForm.fields.iin_own_seller__auto.split(',');
		let autoRegId;
		if(newNumber.length){
			for(let item of newNumber){
				if(item.includes('autoRegNum')){
					autoRegId = item;
				}
			}
		}

		showReg01NextSteps(
			this.contract.inputForm.fields.iin_buyer,
			this.contract.inputForm.fields.iin_seller_seats,
			this.contract.inputForm.fields.summ,
			this.contract.inputForm.fields.iin_own_seller__auto__autoRegNum__,
			autoRegId ? autoRegId : 'autoRegNum1',
			this.id)
	}

	goToStatus(){
		showStatusPage(this.reqNumber, this.id, true)
	}

	getTemplateById(id){
		this.app.get('/documents/' + id)
	    .subscribe(res => {
	    	this.document = res;
	    	if(id === this.app.workflowIDS.auto || id === this.app.workflowIDS.cession || id === this.app.workflowIDS.places || id === this.app.workflowIDS.baspana){
	    		this.getWorkflowState();
	    	}
	    	this.document.inputForm.fields[1].sort(function(obj1,obj2){
	    		return obj1.order - obj2.order;
	    	})
	    });
	}

	dowloadHtml(){
		let data = {
			"documentId": this.contract.documentId,
			"templateId": this.document.templateId,
			"inputForm": {
				"fields": this.documentFileds
			},
			"locale": this.currentLanguage
		}
		this.app.postHTML('/documents/generator/renderHTML', data).subscribe(res => {
			this.html = this.sanitizer.bypassSecurityTrustHtml(res);
		})
	}

	dowloadPDF(){
		let data = {
			"documentId": this.contract.documentId,
			"templateId": this.document.templateId,
			"inputForm": {
				"fields": this.documentFileds
			},
			"locale": this.currentLanguage,
		}
		this.app.postPDF('/documents/generator/renderPDF', data).subscribe(res => {
			this.downloadFile(res)
		})
	}

	downloadFile(data) {
		let today: number = Date.now();
	    let blob = new Blob([data], {type: 'application/vnd.ms-excel'});
	    var blobURL = window.URL.createObjectURL(blob);
	    var anchor = document.createElement("a");
	    anchor.download = "doc"+today+".pdf";
	    anchor.href = blobURL;
	    anchor.click();
  	}

  getXML() {
    const url = '/contracts/' + this.id + '/for_signature';
    this.app.getXML(url).subscribe( res => {
      const data = btoa(encodeURI(res));
      const anchor = document.createElement('a');
      anchor.href = `action://mgov.mobile.kz/module?encoded_xml=${data}`;
      anchor.click();
      setTimeout(() => {
          this.signDocIng = false;
      }, 2000);
      this.signDocIng = false;
    }, error => {
      this.signDocIng = false;
      this.errorText = this.translate.instant('Messages.fetchingError');
      this.errorDescription = error ? error.error.message : this.translate.instant('Messages.Incorrect sign');
      const anchors = document.getElementById('showError');
      anchors.click();
    });
  }

  convertXml(xml: string, prefix: string): string | null {
    const head = xml.substring(0, xml.indexOf(`${prefix}`));
    const body = xml.substring(xml.indexOf(`${prefix}`), xml.length);
    const contract = body.substring(0, body.indexOf('<ds:Signature'));
    const foot = body.substring(body.indexOf('<ds:Signature'), body.length);

    const newContractWithQuote = contract.toString().replace(/"/g, '&quot;');
    const cert1 = (head + newContractWithQuote + foot).replace(/(\r\n|\n|\r)/gm, '\n');
    if (xml.indexOf('login/>') > -1) {
      return null;
    }
    // return cert1.replace(/"/gm, '"');
    return cert1;
  }

  insertXml(xml, password) {
    const data = {
      initiatorSignature: {
        signature : this.convertXml(xml, '<data>'),
        publicKey : password
      }
    };
    return this.app.postData(`/contracts/${this.id}/start`, data).toPromise().then(res => {
      const anchors = document.getElementById('reload');
      anchors.click();
      return 'signed';
    }, error => {
      console.log(error);
      this.signDocIng = false;
      // alert(error.__zone_symbol__value.error.message);
      this.errorText = this.translate.instant('Messages.signingError');
      this.errorDescription = error ? error.error.message : this.translate.instant('Messages.Incorrect sign');
      const anchors = document.getElementById('showError');
      anchors.click();
      return error;
    });
  }

  showError() {
    this.modalError = true;
  }


  	signDoc(){
  		this.signDocIng = true;
  		let data = this.getXML()
  	}


  	signDocIn(){
  		var anchor = document.createElement("a");
  		anchor.href = 'action://mgov.mobile.kz/module?page=xmlinitiator&id=' + this.id;
  		anchor.click();
  	}

  	checkInn(val){
  		let confirmed = this.contract.confirmedParties.filter(({iin}) => iin === val);
  		if(confirmed && confirmed.length){
  			return 'signed';
  		}else{
  			if(this.contract.rejectedParties){
  				let rejected = this.contract.rejectedParties.filter(({party}) => party.iin === val);
  				if(rejected && rejected.length){
  					return 'rejected';
  				}else{
  					return 'waiting';
  				}
  			}else{
  				return 'waiting';
  			}
  		}
  	}


  	deleteDocument(){
  		this.app.post(`/contracts/${this.id}/cancel`,{}).subscribe(res => {
  			this.getContractById();
  			this.deleteModal = false;
  			this.router.navigate(['../'])
  		})
  	}

  	goBack() {
      const backUrl = this.route.snapshot.queryParams.backUrl;
      if (backUrl) {
        this.router.navigate([backUrl]);
      } else {
        (window as any).mobapp.onBackNavigation();
      }
    }

}
