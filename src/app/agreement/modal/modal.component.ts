import {Component, OnInit} from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {AppletService} from '../../_services/applet.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
})
export class ModalComponent implements OnInit {

	doc_id;
	loading = false;
	loadingTin = false;
	error;
	errorSelf;
	findUser;
	disabled = true;
	userIIN;
	userTin;
	organization;
	tinError;
	comments;
	list = [];
	modal = false;
	selectedId;
	servererror = false;

	constructor(
	    public app: AppletService,
	    public router: Router,
	    public translate: TranslateService,
	    private route: ActivatedRoute,
	  ) {
	}

	ngOnInit() {
		this.doc_id = this.route.snapshot.params['id'];
	}


  sendAgreement() {
    this.disabled = true;
    const data = {
      tin : this.userTin,
      initiator : this.app.user.iin,
      doc_id : this.doc_id,
      commentary : this.comments,
      acceptors: []
    };
    for (let item of this.list) {
      data.acceptors.push(item.iin);
    }
    this.app.postAgreement(data).subscribe(res => {
      if (res.code === 0) {
        this.router.navigate(['/contract']);
        this.disabled = false;
      }
    }, error => {
      alert(error);
      this.disabled = false;
    });
  }

	onChange(item){
		this.error = false;
		this.errorSelf = false;
		this.servererror = false;
	}

	searchUser(item) {
    if (item && item.toString().length === 12) {
      // console.log(item,this.app.user.iin)

      this.loading = true;

    } else {
      this.findUser = null;
      this.disabled = true;
    }
  }

  addUser() {
    if (this.userIIN && this.userIIN.toString().length === 12) {
      if (this.userIIN === this.app.getCookie('user_data').iin) {
        this.errorSelf = true;
        return;
      }
      console.log(this.list);
      console.log(this.userIIN);
      if (this.list.find(item => item.iin === this.userIIN)) {
        this.error = true;
        return;
      }
      this.loading = true;
      console.log(this.userIIN);
      this.app.get(`/dictionary/fullName/${this.userIIN}/`).subscribe(res => {
        console.log(res);
        this.findUser = res.elements[0].name.ru;
        this.loading = false;
        const d = {
          FIO: this.findUser,
          iin: this.userIIN
        };
        this.list.push(d);
        this.userIIN = null;
        this.findUser = null;
        this.error = false;
        this.errorSelf = false;
        if (!this.tinError) {
          this.disabled = false;
        }
      }, () => {
        this.loading = false;
        this.servererror = true;
        this.findUser = null;
      });
    }
  }

	removeParties(i){
		this.list.splice(i,1);
		this.modal = false;

	}

	onChangeTin(item){
		this.tinError = '';
		if(item && item.toString().length === 12){
			this.loadingTin = true;
			if(this.list.length){
				this.disabled = false;
			}
			this.app.get('/dictionary/organization/' + item + '/').subscribe(res => {
				this.organization = res.elements;
				this.loadingTin = false;
			},error => {
				this.loadingTin = false
				this.organization = null;
				this.tinError = 'Введен некорректный БИН';
				this.disabled = true;
			})
		}else{
			this.organization = null;
			if(this.list.length){
				this.disabled = false;
			}
		}
	}
}
