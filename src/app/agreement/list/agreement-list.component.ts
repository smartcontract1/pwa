import {Component, OnInit} from '@angular/core';
import {Router,ActivatedRoute, NavigationEnd} from '@angular/router';
import {AppletService} from '../../_services/applet.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import { HostListener } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-agreement-list',
  templateUrl: './agreement-list.component.html',
  styleUrls: ['./agreement-list.component.css'],
})
export class AgreementListComponent implements OnInit {

	tabs = 1;
	list = [];
	modal = false;
	selectedRequest;
	loading = false;
	statuses = {
			'CREATED' : 'Запрошен',
			'ACCEPTOR_REJECTED' : 'Отклонен',
			'REQUESTED': 'Запрошен',
			'SUCCESS' : 'Подтвержден',
			'REJECTED' : 'Отклонен',
			'ACCEPTOR_SIGNED' : 'Подтвержден',
			'USED' : 'Использовано'

		}
	currentLanguage;
	modalError = false;
	constructor(
	    public app: AppletService,
	    public router: Router,
	    public translate: TranslateService,
	    private activatedRoute: ActivatedRoute,
	  ) {
	  	this.app.getCookie('user_data');
	  	for(let item in this.statuses){
	  		this.translate.get(item).subscribe(
		    	translation => {
		        this.statuses[item] = translation;
		    });
	  	}
	  	this.currentLanguage = this.translate.currentLang || this.translate.defaultLang;
	    translate.onLangChange.subscribe((event: LangChangeEvent) => {
	      this.currentLanguage = event.lang;
	    });
	  	eval("window.webv=this;");

	  	router.events.subscribe((val) => {
	        if(val instanceof NavigationEnd){
	        	this.list = [];
	        	this.tabs = this.activatedRoute.snapshot.params['tab'];
				if(this.tabs){
					this.tabs = this.tabs * 1;
				}
				if(this.tabs && this.tabs === 2){
					this.getIncomeAgreements();
				}else{
					this.tabs = 1;
					this.getContractList();
				}
	        }
	    });

	}

	ngOnInit() {

	}

	getXML(id, item){
		item['loading'] = true
		this.selectedRequest = id;
		this.app.getXMLRequest(id).subscribe( res =>{
			console.log(res.result.xml)
			let data = btoa(encodeURI(res.result.xml))
	  		var anchor = document.createElement("a");
	  		anchor.href = 'action://mgov.mobile.kz/module?encoded_xml=' + data;
	  		anchor.click();
	  		setTimeout(()=>{
	  			item['loading'] = false;
			}, 2000);
		},error => {
			// alert(error)
			item['loading'] = false;
		})
  	}

  	insertXml(xml,password,id?){

  		let data =
  			{
				"agreement_id": id ? id : this.selectedRequest,
				"sign_acceptor": xml
			}
		this.loading = true;
		return this.app.signXml(data).toPromise().then(res=>{
	  		var anchors = document.getElementById("reload");
	  		anchors.click()
	  		this.loading = false;
  			return 'signed';
		}, error => {
			this.loading = false;
			var anchors = document.getElementById("showError");
	  		anchors.click()
			return error;
		})
  	}

  	showError(){
		this.modalError = true;
  	}

	getContractList(){
		this.list = [];
		this.loading = true;
		this.app.getAgreements().subscribe(res => {
			for(let item of res.result.results){
				for(let elem in item){
					if( elem === 'full_name_acceptor' || elem === 'title' ){
						item[elem] = JSON.parse(item[elem].replace(/\'/gi,'"'));
					}
				}
			}
			this.list = res.result.results;
			this.loading = false;
		})
	}

	rejectRequest(){
		this.app.rejectRequest(this.selectedRequest).subscribe(res => {
			this.getIncomeAgreements();
			this.modal = false;
		})
	}

	getIncomeAgreements(){
		this.list = [];
		this.loading = true;
		this.app.getIncomeAgreements().subscribe(res => {
			for(let item of res.result.results){
				for(let elem in item){
					if( elem === 'full_name_initiator' || elem === 'title'){
						item[elem] = JSON.parse(item[elem].replace(/\'/gi,'"'));
					}
				}
			}
			this.list = res.result.results;
			this.loading = false;
		})
	}

	getStatus(status){
		return  this.statuses[status];
	}

	// capitalize = (str: string): string => {
	// 	console.log(str.replace(/\b\w/g, function(l){ return l.toUpperCase() }))
	//     return str.replace(/\b\w/g, function(l){ return l.toUpperCase() })
	// };

}
