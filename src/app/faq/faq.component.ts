import {Component, OnInit} from '@angular/core';
import {AppletService} from '../_services/applet.service';
import {Router, ActivatedRoute,NavigationEnd} from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css'],
})
export class FaqComponent implements OnInit{
	currentLanguage;

	slide = 1;

	constructor(
    public app: AppletService,
    public router: Router,
    public translate: TranslateService,
    private activatedRoute: ActivatedRoute, 
	  ){
	    this.app.getCookie('user_data');
	    this.currentLanguage = this.translate.currentLang || this.translate.defaultLang;
	    translate.onLangChange.subscribe((event: LangChangeEvent) => {
	      this.currentLanguage = event.lang;
	    });
	}

	ngOnInit(){
		
	}

}