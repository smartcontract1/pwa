import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router, ParamMap} from '@angular/router';
import {AppletService} from '../_services/applet.service';
import { DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-parties',
  templateUrl: './parties.component.html',
  styleUrls: ['./parties.component.css'],
})
export class PartiesComponent {
	id;
	list = [];

	newUserIIN;
	findUser;
	disabled = true;
	contract;
	error = false;
	modal = false;
	selectedId = null;
	loading = false;
	errorSelf = false;
	constructor(private route: ActivatedRoute,
      		public app: AppletService,
      		public router: Router,
      		private sanitizer: DomSanitizer) {
	}

	ngOnInit() {
		this.id = this.route.snapshot.params['id'];
		this.getContractById();
		// this.getPartiesById()
	}

	// getPartiesById(){
	// 	this.app.get('/contracts/' + this.id + '/parties').subscribe(res => {
			// if(Object.keys(res).length){
			// 	let d: any = res;
			// 	this.list = d;
			// 	for(let item of res){
			// 		item['FIO'] = this.returnUser(item.iin);
			// 	}
			// }else{
			// 	this.list = [];
			// }
	// 	})
	// }

	getContractById(){
		this.app.get('/contracts/' + this.id)
	    .subscribe(res => {
	    	this.contract = res;
	    	if(Object.keys(this.contract.parties).length){
				let d: any = this.contract.parties;
				this.list = d;
				for(let item of this.contract.parties){
					item['FIO'] = this.returnUser(item.iin);
				}
			}else{
				this.list = [];
			}
	    	// console.log(this.contract.parties)
	    	// this.contract.parties
	    	// this.getPartiesById();
	    });
	}

	addUser(){
		if(this.newUserIIN && this.newUserIIN.toString().length == 12){
			this.loading = true;
			if(!this.list.filter(item => item.iin * 1 === this.newUserIIN * 1)[0] && this.newUserIIN !== this.app.user.iin){
				let d = {
					FIO: this.findUser,
					iin: this.newUserIIN
				}
				// this.list.push(d);
				this.saveParties(d);
			}else if(this.newUserIIN === this.app.user.iin){
				this.errorSelf = true;
				this.loading = false;
			}else{
				this.error = true;
				this.loading = false;
			}
		}
	}

	onChange(item){
		this.error = false;
		this.errorSelf = false;
		if(item && item.toString().length === 12){
			this.loading = true;
			this.app.get('/dictionary/fullName/' + item + '/').subscribe(res => {
				this.findUser = res.elements[0].name.ru;
				this.disabled = false;
				this.loading = false;
			},error => {
				this.loading = false
				this.disabled = true;
				this.findUser = null;
			})
		}else{
			this.findUser = null;
			this.disabled = true;
		}
	}

	returnUser(iin){
		let user = "";
		if(this.contract && this.contract.partiesFullNames[iin]){
			user = this.contract.partiesFullNames[iin].last + ' ' + this.contract.partiesFullNames[iin].first + ' ' + this.contract.partiesFullNames[iin].middle;
		}
		if(!user){
			this.app.get('/shep/gbdfl/person?iin=' + iin).subscribe(res => {
				user = res.fullNameLang.ru;
			})
		}
		return user;
	}

	saveParties(d){
		let b = [];
		var copy = Object.assign([], this.list);
		console.log(copy)
		copy.push(d);
		for(let item of copy){
			b.push({iin:item.iin});
		}
		this.app.put('/contracts/' + this.id + '/parties', {parties: b}).subscribe(res => {
			this.getContractById();
			this.disabled = true;
			this.newUserIIN = null;
			this.findUser = null;
			this.loading = false;
			this.error = false;
			this.errorSelf = false;
		},error => {
			this.loading = false;
		})
	}

	removeParties(i){
		// this.list.splice(i,1);
		let data = {
			"parties": [
		        {
		            "iin": this.list[i].iin
		        },
		    ]
		}
		this.app.delete('/contracts/' + this.id + '/parties/',data).subscribe(res => {
			// this.getPartiesById();
			this.list.splice(i,1);
			this.modal = false;
		})
	}
}