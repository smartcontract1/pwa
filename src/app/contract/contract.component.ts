import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AppletService} from '../_services/applet.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.css'],
})
export class ContractComponent implements OnInit {
	templates;
	showTemplates = false;
	currentLanguage = 'ru';

	constructor(
    public app: AppletService,
    public router: Router,
    public translate: TranslateService
  ) {
    const lang = this.app.getCookie('lang');

    this.currentLanguage =  lang ? lang : 'ru';
    if (lang === 'en') {
      this.currentLanguage = 'ru';
    }
    this.app.getCookie('user_data');
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
      if (event.lang === 'en') {
        this.currentLanguage = 'ru';
      }
    });
	}

	ngOnInit() {
		this.getTemplates();
	}

	getTemplates(){
		this.app.get('/documents/read?start=0&limit=10&state=ACTIVE')
	    .subscribe(res => {
	    	this.templates = res;
	    });
	}

	getTemplate(documentId){
		this.app.get('/documents/' + documentId)
	    .subscribe(res => {
	    	var anchor = document.createElement("a");
	  		anchor.href = `action://mgov.mobile.kz/module?page=pdfsample&id=${res.templateId}&language=${this.currentLanguage}`;
	  		anchor.click();
	    });
	}
}
