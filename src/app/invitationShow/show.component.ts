import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppletService} from '../_services/applet.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import  '../../assets/js/custom.js';
declare function showStatusPage(requestNumber, contractId, seller): any;

@Component({
  selector: 'app-show-inv',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css'],
})
export class InvitationShowComponent {
	id;
	contract;
	document;
	documentFileds = {};
	html;
	loading = false;
  	currentLanguage = 'ru';
  	showStatusBtn = false;
  	reqNumber;

  	rkCode;
	showPayedText = false;
	showBtn = false;
	showInfoText = false;

	rejectModal = false;
	showPaymentText = false;
	inProgressText = false;
	signDocIng = false;
	modalError = false;

	constructor(
	    public app: AppletService,
	    public router: Router,
	    public translate: TranslateService,
	    private route: ActivatedRoute,
	  ) {
		app.getCookie('user_data');
	 this.currentLanguage = this.translate.currentLang || this.translate.defaultLang;
	 translate.onLangChange.subscribe((event: LangChangeEvent) => {
	      this.currentLanguage = event.lang;
	    });
	 eval('window.webv=this;');
	}

	ngOnInit() {
		this.id = this.route.snapshot.params.id;
		this.getContractById();
	}

	getContractById() {
		this.app.get('/contracts/' + this.id)
	    .subscribe(res => {
	    	this.contract = res;
			   this.documentFileds = this.contract.inputForm.fields;
			   this.getTemplateById(this.contract.documentId);
	    });
	}

	getContractByIdLoad() {
		this.loading = true;
		this.app.get('/contracts/' + this.id)
	    .subscribe(res => {
			this.contract = res;
			// this.getWorkflowState();
			if (this.contract.currentState === 'VERIFICATION' || this.contract.currentState === 'PARTIES_SIGNATURE_VERIFICATION' || this.contract.currentState === 'WAITING_FOR_APPROVAL') {
				setTimeout(() => {
					this.getContractByIdLoad();
			 	}, 3000);
			} else {
				this.loading = false;
				this.documentFileds = this.contract.inputForm.fields;
				this.getTemplateById(this.contract.documentId);
			}
	    });
	}

	getTemplateById(id) {
		this.app.get('/documents/' + id)
	    .subscribe(res => {
	    	this.document = res;
	    	if (id === this.app.workflowIDS.auto || id === this.app.workflowIDS.cession || id === this.app.workflowIDS.places || id === this.app.workflowIDS.baspana) {
	    		this.getWorkflowState();
	    	}
	    	this.document.inputForm.fields[1].sort(function(obj1, obj2) {
	    		return obj1.order - obj2.order;
	    	});
	    });
	}

	rejectedDoc() {
		const data = { parties:
		    [{
		      party: {
		        iin: this.app.user.iin
		      },
		      reason: ''
		    }]};
		this.app.rejectDocument(this.id, data).subscribe(res => {
			this.getContractByIdLoad();
			this.rejectModal = false;
		});
	}

	payC() {
		this.app.payWorkflowState(this.id).subscribe(res => {
			this.getContractByIdLoad();
		});
	}



getWorkflowState() {
		let showBtn = false;
		this.showStatusBtn = false;
		this.rkCode = null;
		this.showPayedText = false;
		let rk;
		this.inProgressText = false;
		this.app.getWorkflowState(this.id).subscribe(res => {
			for (const item of res.data.tasks) {
				if (item.status === 'IN_PROGRESS' && item.taskType !== 'wait_for_initiator_sign' && item.taskType !== 'wait_for_acceptor_sign' && item.taskType !== 'wait_for_mu_payment') {
					this.inProgressText = true;
				}
				if (item.taskType === 'wait_for_reg01_start') {
					if (item.status === 'COMPLETED') {
						this.reqNumber = item.outputData.request_number;
						this.showInfoText = true;
					} else if (item.status === 'IN_PROGRESS') {
						showBtn = true;
						this.showInfoText = true;
					}
				}

				if (item.taskType === 'wait_for_my_payment') {
					if (item.status === 'COMPLETED' || item.status === 'IN_PROGRESS') {
						this.showInfoText = false;
					}
				}

				if (item.taskType === 'wait_for_reg01_finish') {
					if (item.status === 'COMPLETED') {

					} else if (item.status === 'IN_PROGRESS') {
						this.showStatusBtn = true;
					}
				}

				if (item.taskType === 'get_rk_code') {
					if (item.status === 'COMPLETED') {
						rk = item.outputData.rk_code;
					}
				}


				if (item.taskType === 'wait_for_mu_payment') {
					if (item.status === 'IN_PROGRESS') {
						this.rkCode = rk;
					} else if (item.status === 'COMPLETED') {
						this.showPayedText = true;
					}
				}

				if (item.taskType === 'send_mu') {

				}
			}
			if (showBtn && !this.showStatusBtn) {
				this.showBtn = true;
			}
			this.showPaymentText = true;
		});
	}

	goToStatus() {
		showStatusPage(this.reqNumber, this.id, false);
	}

  	getXML() {
  		const url = '/contracts/' + this.id + '/for_signature';
		  this.app.getXML(url).subscribe( res => {
			const data = btoa(encodeURI(res));
	  const anchor = document.createElement('a');
	  anchor.href = 'action://mgov.mobile.kz/module?encoded_xml=' + data;
	  anchor.click();
	  setTimeout(() => {
	  			this.signDocIng = false;
			}, 2000);
		}, error => {
			this.signDocIng = false;
		});
  	}

  	insertXml(xml, password) {
  		const data = {
  			parties : [
  				{
  					party: {
  						iin : this.app.user && this.app.user.iin
  					},
		  			signature: {
						signature : xml,
						publicKey : password
		  			}
  				}
  			]
  		};

		  this.app.postData(`/contracts/${this.id}/parties/confirm`, data).subscribe(res => {
	  		const anchors = document.getElementById('reload');
	  		anchors.click();
	  		this.signDocIng = false;
  			return 'signed';
		}, error => {
			// alert(error.message)
			this.signDocIng = false;
			const anchors = document.getElementById('showError');
	  anchors.click();
			return 'error';
		});

  	}

  	showError() {
		this.modalError = true;
  	}

  	signDoc() {
  		this.signDocIng = true;
  		const data = this.getXML();
  	}

	signDocIn() {
  		const anchor = document.createElement('a');
  		anchor.href = 'action://mgov.mobile.kz/module?page=xmlinitiator&id=' + this.id;
  		anchor.click();
  	}

  	checkInn(val) {
  		const confirmed = this.contract.confirmedParties.filter(({iin}) => iin === val);
  		if (confirmed && confirmed.length) {
  			return 'signed';
  		} else {
  			if (this.contract.rejectedParties) {
  				const rejected = this.contract.rejectedParties.filter(({party}) => party.iin === val);
  				if (rejected && rejected.length) {
  					return 'rejected';
  				} else {
  					return 'waiting';
  				}
  			} else {
  				return 'waiting';
  			}
  		}
  	}
}
