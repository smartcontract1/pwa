import { Component } from '@angular/core';
import {AppletService} from './_services/applet.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(
    public app: AppletService,
    private translate: TranslateService
  ) {
    app.getCookie('user_data');
    const lang = app.getCookie('lang');
    if (lang) {
      translate.setDefaultLang(lang === 'en' ? 'ru' : lang);
    } else {
      translate.setDefaultLang('kk');
    }
  }
}
