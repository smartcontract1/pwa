import * as moment from 'moment';

export const defaultDateFormat = 'DD.MM.YYYY';

export const transformDate = (date: string): string => {
  const md = moment(date, [defaultDateFormat, 'YYYY.MM.DD', 'YYYY-MM-DD']);
  return md.isValid() ? md.format(defaultDateFormat) : date;
};
