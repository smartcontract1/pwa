import {Component, OnInit} from '@angular/core';
import {AppletService} from '../_services/applet.service';
import {Router, ActivatedRoute} from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import {forkJoin} from 'rxjs';

@Component({
  selector: 'app-mainlist',
  templateUrl: './mainlist.component.html',
  styleUrls: ['./mainlist.component.css'],
})
export class MainListComponent implements OnInit {
  currentLanguage;
  unreadCount = {
    party: 0,
    initiator: 0,
    personalData: 0,
  };

  constructor(
    public appletService: AppletService,
    public router: Router,
    public translate: TranslateService,
  ) {
    this.appletService.getCookie('user_data');
    this.currentLanguage = this.translate.currentLang || this.translate.defaultLang;
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
    });
  }

  ngOnInit() {
    this.getUpdatesCount();
  }

  private getUpdatesCount() {
    const contract = this.appletService.getUpdatesCount();
    const agreement = this.appletService.getUnreadAgreements();
    forkJoin([contract, agreement]).subscribe(res => {
      [this.unreadCount] = res[0];
      this.unreadCount.personalData = res[1].result.unread_acceptor_count;
    });
  }

  getCookies() {
    this.appletService.getCookie('user_data');
  }
}
