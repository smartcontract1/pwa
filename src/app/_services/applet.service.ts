import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import '../../assets/js/custom.js';
import {environment} from '../../environments/environment';

declare function sessionExpired(): any;

@Injectable()
export class AppletService {
  public user;
  public sso;

  baseUrl = `${environment.apiUrl}/${environment.version}`;
  agreementUrl = `${environment.apiUrl}/agreement`;
  workFlowUrl = environment.apiUrl;


  workflowIDS = {
    auto: 'a013436b-0d95-468f-b065-84f18e7b7c19',
    cession: '0acb8a03-3eba-4122-a6b5-30ca9112e4d4',
    places: '654e69a3-750f-438e-87f4-27edc3dc8785', // залог?
    baspana: 'dcb172ed-6d47-4ba0-b609-47c88d52efcf'
  };

  private httpOptions = {
    headers: new HttpHeaders({
      Accept: 'text/html',
      'Content-Type': 'application/json'
    }),
    responseType: 'text'
  };


  constructor(private http: HttpClient) {
    this.getCookie('user_data');
  }

  get(url): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.get(this.baseUrl + url, { headers })
      .pipe(
        // retry(1),
        catchError(this.handleError)
      );
  }

  getUpdatesCount(): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.get( `${this.baseUrl}/contracts/read/count-unread/${this.user.iin}`, { headers })
      .pipe(catchError(this.handleError));
  }
  getUnreadAgreements(): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.get(`${environment.apiUrl}/agreement/unread_count/`, { headers, params: { acceptor: this.user.iin } });
  }

  getAgreements(): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    const params = {
      limit: '100',
      ...(this.user && {initiator: this.user.iin})
    };
    return this.http.get(this.agreementUrl, {headers, params})
      .pipe(
        catchError(this.handleError)
      );
  }

  signXml(data): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.post(this.agreementUrl + '/sign_acceptor/', data, {headers}).pipe(
      // retry(1),
      catchError(this.handleError)
    );
  }

  getIncomeAgreements(): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    const params = {
      limit: '100',
      ...(this.user && {acceptor: this.user.iin})
    };
    return this.http.get(this.agreementUrl, {headers, params})
      .pipe(
        catchError(this.handleError)
      );
  }

  getXMLRequest(id): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.get(this.agreementUrl + `/${id}/get_xml/`, {headers}).pipe(
      catchError(this.handleError)
    );
  }

  rejectRequest(id): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.get(this.agreementUrl + `/${id}/reject_acceptor/`, {headers}).pipe(
      // retry(1),
      catchError(this.handleError)
    );
  }

  getKato(id): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.get(this.baseUrl + `/dictionary/kato/${id}/`, {headers}).pipe(
      // retry(1),
      catchError(this.handleError)
    );
  }

  rejectDocument(contractId, data): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.post(this.baseUrl + `/contracts/${contractId}/parties/reject`, data, {headers}).pipe(
      // retry(1),
      catchError(this.handleError)
    );
  }

  delete(url, content): Observable<any> {
    // console.log(url, content)
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        sso: this.getSSO('sso_token')
      }),
      body: content
    };
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.delete(this.baseUrl + url, options);
  }

  getl(url) {
    const options = {
      headers: new HttpHeaders({
        initiator: this.user.iin,
        sso: this.getSSO('sso_token')
      })
    };
    return this.http.get(this.baseUrl + url, options);
  }

  getDictionary(name: string, params: any): Observable<any> {
    const headers = new HttpHeaders({
        initiator: this.user.iin,
        sso: this.getSSO('sso_token')
      });

    return this.http.get(`${this.baseUrl}/dictionary/${name}`, {headers, params});
  }


  postWorkflow(data): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.post(this.workFlowUrl + '/conductor', data, {headers});
  }

  getWorkflowState(id): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    console.log(headers);
    return this.http.get(this.workFlowUrl + '/conductor/' + id, {headers});
  }

  payWorkflowState(id): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.post(this.workFlowUrl + `/conductor/${id}/pledge_workflow/mu_pay`, {headers});
  }

  // 1
  post(url, data) {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.post(this.baseUrl + url, data, {headers});
  }

  postAgreement(data): Observable<any> {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.post(this.agreementUrl + '/', data, {headers});
  }

  postData(url, data) {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.post(this.baseUrl + url, data, {headers});
  }

  postHTML(url, data) {
    let headers = new HttpHeaders().set('Accept', 'text/html');
    headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.post(this.baseUrl + url, data, {headers, responseType: 'text'});
  }

  postPDF(url, data) {
    let headers = new HttpHeaders().set('Accept', 'text/html');
    headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.post(this.baseUrl + url, data, {headers, responseType: 'blob'});
  }

  put(url, data) {
    const headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.put(this.baseUrl + url, data, {headers});
  }

  getXML(url) {
    let headers = new HttpHeaders().set('Accept', 'application/json, text/plain, */*');
    headers = new HttpHeaders().set('sso', this.getSSO('sso_token'));
    return this.http.get(this.baseUrl + url, {headers, responseType: 'text'});
  }

  b64DecodeUnicode(str: string): string {
    if (window
      && 'atob' in window
      && 'decodeURIComponent' in window) {
      return decodeURIComponent(Array.prototype.map.call(atob(str), (c) => {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      }).join(''));
    } else {
      return null;
    }
  }

  getCookie(name: string) {
    const ca: Array<string> = document.cookie.split(';');
    const caLen: number = ca.length;
    const cookieName = `${name}=`;
    let c: string;
    this.user = null;
    for (let i = 0; i < caLen; i += 1) {
      c = ca[i].replace(/^\s+/g, '');
      if (c.indexOf(cookieName) === 0) {
        try {
          if (name === 'user_data') {
            this.user = JSON.parse(this.b64DecodeUnicode(c.substring(cookieName.length, c.length)));
            return this.user;
          } else {
            const matches = document.cookie.match(new RegExp(
              '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
          }
        } catch (e) {
          if (name === 'user_data') {
            this.user = JSON.parse(c.substring(cookieName.length, c.length));
            return this.user;
          } else {
            const matches = document.cookie.match(new RegExp(
              '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
          }
        }
      }
    }
  }

  getSSO(name) {
    // return this.auth().subscribe(res => {
    //     return res.ticket;
    // })
    // return this.sso;
    // return '563b1c60-a0cc-43a2-8d2e-98d11de2b805';
    let matches = document.cookie.match(new RegExp(
      '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ));
    if (!matches) {
      name = 'SSO';
      matches = document.cookie.match(new RegExp(
        '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
      ));
      if (!matches) {
        name = 'sso_data';
        matches = document.cookie.match(new RegExp(
          '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
        ));
      }
    }
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }


  handleError(error) {
    if (error.status === 401) {
      sessionExpired();
    }
    return throwError(error.status);
  }
}
