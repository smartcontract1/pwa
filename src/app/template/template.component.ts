import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppletService} from '../_services/applet.service';
import {FormBuilder} from '@angular/forms';

import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import {defaultDateFormat, transformDate} from '../_share/constants';
import * as moment from 'moment';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css'],
})
export class TemplateComponent implements OnInit {
  id;
  templateId;
  document;
  documentFields = {
    my_full_name: '',
    my_iin: ''
  };
  demo;
  contractId;
  selects = [];
  findUser;
  loading = false;
  currentLanguage = 'ru';

  doc;
  contract;
  creating = false;
  saveClicked = false;
  currentDate;
  public dynamicListSources = {};
  scrolled = false;
  showKatoModal = false;
  katos = [];
  katoField;
  private katoNames = [];

  constructor(
    private route: ActivatedRoute,
    public app: AppletService,
    public router: Router,
    private fb: FormBuilder,
    public translate: TranslateService
  ) {
    const lang = this.app.getCookie('lang');
    this.currentLanguage =  lang ? lang : 'ru';
    if (lang === 'en') {
      this.currentLanguage = 'ru';
    }
    this.app.getCookie('user_data');
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
      if (event.lang === 'en') {
        this.currentLanguage = 'ru';
      }
    });
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.currentDate = moment(new Date()).format(defaultDateFormat);
    this.templateId = this.route.snapshot.params.templateId;
    this.id = this.route.snapshot.params.id;
    this.id ? this.getContractById() : this.getTemplateById();
  }

  getContractById() {
    this.app.get('/contracts/' + this.id)
      .subscribe(res => {
        this.contract = res;
        this.documentFields = this.contract.inputForm.fields;
        this.getTemplateById();
      });
  }

  getTemplateById() {
    this.app.get('/documents/' + this.templateId)
      .subscribe(res => {
        this.document = res;
        this.document.inputForm.fields['1'].sort((obj1, obj2) => {
          return obj1.order - obj2.order;
        });
        for (const field of this.document.inputForm.fields['1']) {
          if (!this.id) {
            this.documentFields[field.code] = null;
          }
          if (field.source && field.sourceType === 'STATIC') {
            this.getDic(field.source);
          }
          if ( (!field.sourceType) && (field.targetFields && field.targetFields[0])) {
            this.dynamicListSources[field.code] = { elements: []};
          }
          if (field.dataType === 'DATE_TIME' || field.dataType === 'LOCAL_DATE') {
            if (this.documentFields[field.code]) {
              this.documentFields[field.code] = transformDate(this.documentFields[field.code]);
            }
          }
          if (field.code.includes('iin_own') && field.source) {
            if (field.code !== 'iin_own_seller__auto' && field.code !== 'iin_own_lessor__estates') {
              this.documentFields[field.code] = this.app.user.iin;
            }
            this.onChange(field.source, this.documentFields[field.code], field);
          }

          if (field.source && ( (field.sourceType === 'DYNAMIC') || (!field.sourceType))) {
            if (!this.dynamicListSources[field.code]) {
              if ( this.documentFields && this.documentFields[field.code]) {
                this.onChange(field.source, this.documentFields[field.code] , field);
              }
            }
          }
          if (field.defaultValue) {
            this.documentFields[field.code] = field.defaultValue;
          }
        }
        this.organizeDateFields();
      });
  }

  getFieldByCode(code) {
    return this.document.inputForm.fields[1].find(f => f.code === code);
  }

  organizeDateFields() {
    Object.keys(this.documentFields).map((code: string) => {
      const field = this.getFieldByCode(code);
      if (field && (field.dataType === 'DATE_TIME' || field.dataType === 'LOCAL_DATE')) {
        const date = this.documentFields[field.code];
        const event = {
          date: {
            day: moment(date, ['DD.MM.YYYY']).date(),
            month: moment(date, ['DD.MM.YYYY']).month() + 1,
            year: moment(date, ['DD.MM.YYYY']).year(),
          },
          val: date
        };
        const today: NgbDateStruct = {
          day: moment().date(),
          month: moment().month() + 1,
          year: moment().year(),
        };
        // Check for current date:
        if (field.dateGreaterThan === 'current_date') {
          field.minDate = today;
        }
        if (field.dateLessThan === 'current_date') {
          field.maxDate = today;
        }
        this.onDateChange(event, field);
      }
    });
  }
  onSelectKato(field, kato) {
    this.katoNames.push(kato);
    this.documentFields[field.code] = kato.id;
    this.documentFields[field.targetFields[0]] = this.katoNames.map(obj => obj.name[this.currentLanguage]).join(', ');

    this.showKatoModal = false;
    this.katos = [];
    this.katoNames = [];
  }
  showModalKato(field, kato?) {
    this.katoField = field;
    // TODO: remove this hardcode:
    if (!kato || !kato.name.ru.includes('город')) {
      this.loading = true;
      this.katos = [];
      this.showKatoModal = true;
      if (kato) {
        this.katoNames.push(kato);
      }
      this.app.get(`/dictionary/kato/${kato ? kato.id : ''}`).subscribe(res => {
        this.katos = res.elements;
        this.loading = false;
        if (!res.elements.length) {
          this.onSelectKato(field, kato);
          this.showKatoModal = false;
        }
      });
    }
  }

  getDic(item) {
    const params = this.id ? {contractId: this.id} : {};

    if (item === 'estates') {
      this.app.getDictionary(`${item}/${this.app.user.iin}`, {doc_id: this.document.documentId, ...params})
        .subscribe((res: any) => {
          this.selects[item] = res.elements;
        });
    } else {
      this.app.getDictionary(item, params).subscribe((res: any) => {
        this.selects[item] = res.elements;
      });
    }
  }

  checkvalidit() {
    this.saveClicked = true;
    let count = 0;
    this.scrolled = false;
    for (const item of this.document.inputForm.fields['1']) {
      if (this.documentFields[item.code] && !item.readOnly) {
        if (item.validator) {
          const d = this.checkInvalid(this.documentFields[item.code], item.validator);
          // item.errorMessage this.currentLanguage
          if (d) {
            count++;
            item.showError = true;
            this.saveClicked = false;
            if (count === 1  && item.display) {
              this.scrollToElem(item);
            } else if (count === 2 && item.display) {
              this.scrollToElem(item);
            } else if (count === 3 && item.display) {
              this.scrollToElem(item);
            }
          }
        }
      } else if (!this.documentFields[item.code] && !item.readOnly) {
        count++;
        item.showError = true;
        this.saveClicked = false;
        if (count === 1 && item.display) {
          this.scrollToElem(item);
        } else if (count === 2 && item.display) {
          this.scrollToElem(item);
        } else if (count === 3 && item.display) {
          this.scrollToElem(item);
        }
      } else {
        item.showError = false;
      }
    }
    if (count === 0) {
      this.createContract();
    }
  }


  scrollToElem(item) {
    if (!this.scrolled) {
      document.getElementById('Field' + item.code).scrollIntoView({
        behavior: 'smooth',
        block: 'center',
      });
      this.scrolled = true;
    }
  }

  createContract() {
    this.creating = true;
    this.documentFields.my_iin = this.app.user.iin;
    this.documentFields.my_full_name = this.app.user.fullName;
    const data = Object.assign({}, this.documentFields);
    const d = {
      documentId: this.templateId,
      initiator: {
        iin: this.app.user.iin
      },
      inputForm: {
        fields: data
      },
      language: this.currentLanguage,
      initiatorSignature: 'signature',
    };

    if (!this.id) {
      this.app.post('/contracts/', d).subscribe(res => {
        this.contractId = res;
        this.creating = false;
        this.saveClicked = false;
        if (this.app.workflowIDS.auto === this.templateId) {
          const postData = {
              workflow_type: 'reg_01_workflow',
              contract_id: this.contractId.id
          };
          this.app.postWorkflow(postData).subscribe(() => {
            this.router.navigate(['/show/' + this.contractId.id]);
          });
        } else if (
          this.app.workflowIDS.cession === this.templateId
          || this.app.workflowIDS.places === this.templateId
          || this.app.workflowIDS.baspana === this.templateId
        ) {
          const postData2 = {
              workflow_type: 'pledge_workflow',
              contract_id: this.contractId.id
          };
          this.app.postWorkflow(postData2).subscribe(() => {
            this.router.navigate(['/show/' + this.contractId.id], {queryParams: {backUrl: '/main/1'}});
          });
        } else {
          this.router.navigate(['/show/' + this.contractId.id], {queryParams: {backUrl: '/main/1'}});
        }

      }, error => {
        this.creating = false;
        this.saveClicked = false;
      });
    } else {
      this.app.put(`/contracts/${this.id}/input_form`, d).subscribe(res => {
        this.router.navigate(['/show/' + this.id]);
        this.creating = false;
        this.saveClicked = false;
      }, error => {
        this.creating = false;
        this.saveClicked = false;
      });
    }
  }

  onChangeSelect(item, selected) {
    if (selected) {
      const selectedOption = this.selects[item.source].find(o => o.id === selected);
      if (item.targetFields) {
        for (const i of item.targetFields) {
          this.documentFields[i] = selectedOption.name[this.currentLanguage];
        }
      }
      if (item.source === 'cities') {
        this.app.getKato(selected).subscribe(res => {

        });
      } else {
        this.app.get(`/dictionary/${item.source + '_' + selected}`).subscribe(res => {
          for (const elem of res.elements) {
            this.documentFields[item.code + elem.id] = elem.name.ru;
          }
        });
      }

    }
  }

  onSelectDynamicList(field, val) {
    if (field.source === 'estates') {
      // if there ar we pass target fields value directly
      const option = this.dynamicListSources[field.code].elements.find(i => Number(i.id) === Number(val));
      this.documentFields[field.targetFields[0]] = option.name[this.currentLanguage];
    } else {
      const iin = this.dynamicListSources[field.code].source;
      const rk = val;
      const sources = field.source.split(',');
      sources.map(src => {
        const url = `/dictionary/${src}/${iin}/${rk}`;
        this.getSource(src, event, field, url);
      });
    }
  }

  onChange(source, event, item) {
    const urlNew = this.id ? `contractId=${this.id}` : false;
    if (item.serverError) {
      item.serverError = null;
    }
    if ((item.length && event && event.toString().length === item.length) || !item.length) {
      item.loading = true;
      const sources = source.split(',');
      sources.map(src => {
        const url = `/dictionary/${src}/${event}/?doc_id=${this.document.documentId}` + (urlNew ? `&${urlNew}` : '');
        this.getSource(src, event, item, url);
      });
    } else {
      for (const elem in this.documentFields) {
        if (elem !== item.code) {
          if (elem.substring(0, item.code.length) === item.code) {
            this.documentFields[elem] = null;
            if (this.dynamicListSources[elem]) {
              this.dynamicListSources[elem].elements = null;
            }
          }
        }
      }
    }
  }

  getSource(source, event, item, url) {
    this.app.getl(url).subscribe((res: any) => {
      this.selects[event] = res.elements;
      if ((item.targetFields && item.targetFields[0]) || res.dictionaryType !== 'DYNAMIC') {
          for (const el of res.elements) {
            const fieldName = item.code + el.id + '';
            if (fieldName !== '' && this.documentFields[fieldName] !== undefined) {
              if (this.isValidDate(el.name[this.currentLanguage])) {
                this.documentFields[fieldName] = moment(el.name[this.currentLanguage]).format(defaultDateFormat);
              } else {
                this.documentFields[fieldName] = el.name[this.currentLanguage];
              }
            }
          }
          const fieldCode = `${item.code}__${res.id}`;
          this.dynamicListSources[fieldCode] = {
            elements: res.elements,
            source: event
          };
          if (
            !res.dictionaryType ||
            (item.targetFields && item.targetFields[0] && this.documentFields.hasOwnProperty(item.targetFields[0]))
          ) {
            this.documentFields[item.targetFields[0]] = res.elements[0].name[this.currentLanguage]; // todo: check this
          }
      } else {
          if (
            item.targetFields &&
            item.targetFields[0] &&
            item.targetFields[0] &&
            this.documentFields.hasOwnProperty(item.targetFields[0])
          ) {
            this.documentFields[item.targetFields[0]] = res.elements[0].name[this.currentLanguage];
          }
          for (const el of res.elements) {
            const fieldName = item.code + el.id + '';
            if (fieldName !== '' && this.documentFields[fieldName] !== undefined) {
              if (this.isValidDate(el.name[this.currentLanguage])) {
                this.documentFields[fieldName] = moment(el.name[this.currentLanguage]).format(defaultDateFormat);
              } else {
                this.documentFields[fieldName] = el.name[this.currentLanguage];
              }
            }
          }
      }
      item.loading = false;
    }, error => {
      item.loading = false;
      if (error.error && error.error.developerMessage) {
        if (error.error.developerMessage === 'PERMISSION_DENIED') {
          item.serverError = 'Для добавления участника необходимо получить его согласие на сбор и обработку персональных данных';
        } else if (item.errorMessage && item.errorMessage[this.currentLanguage]) {
          item.serverError = item.errorMessage[this.currentLanguage];
        }
      } else {
        if (item.errorMessage && item.errorMessage[this.currentLanguage]) {
          item.serverError = item.errorMessage[this.currentLanguage];
        }
      }
      for (const elem in this.documentFields) {
        // Below code set empty fields that come from dynamic fields, but exception is estates.
        if (elem !== item.code && !item.code.includes('estates')) {
          if (elem.substring(0, item.code.length) === item.code) {
            this.documentFields[elem] = null;
          }
        }
      }
    });
  }

  isValidDate(date) {
    const parsedDate = Date.parse(date);
    if (isNaN(date) && !isNaN(parsedDate)) {
        return true;
    } else if (date.length === 16) {
      const dateN = date.substr(0, 10);
      const parsedDateN = Date.parse(dateN);
      return isNaN(dateN) && !isNaN(parsedDateN);
    } else {
      return false;
    }
  }

  checkInvalid(item, validator) {
    if (validator) {
      const regex = new RegExp(validator);
      return !regex.test(item);
    }
  }

  getFieldStatusClass(field, type = 'input') {
    const currentValue = this.documentFields[field.code];
    const valid = field.validator ? !!currentValue.toString().match(field.validator) : true;
    const mandatory = (field.mandatory && field.display) ? currentValue && currentValue.toString().length > 0 : true;

    return {
      'is-loading': field.loading,
      'is-invalid': !(valid && mandatory),
      'is-valid': valid && mandatory
    };
  }

  onDateChange(event, field) {
    // Process max and min dates with ralated fields:
    this.documentFields[field.code] = event.val;
    const fieldObjects = this.document.inputForm.fields['1'];
    const relatedLess = fieldObjects.find(f => f.dateLessThan === field.code);
    if (relatedLess) {
      relatedLess.maxDate = event.date;
    }
    const relatedGreater = fieldObjects.find(f => f.dateGreaterThan === field.code);
    if (relatedGreater) {
      relatedGreater.minDate = event.date;
    }
  }

  onChangePhone($event: any, field: any) {
    this.documentFields[field.code] = $event.target.value;
    field.showError = this.checkInvalid($event.target.value, field.validator);
  }
}
